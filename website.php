<?php
return [
        'name' => 'Laundry System2',
        'menu_setting' => 'developer',
        'logo' => 'core_logo.png',
        'project' => 'core',
        'favicon' => 'core_favicon.png',
        'error' => 'error.png',
        'backend' => 'porto',
        'frontend' => '',
        'seo' => 'seo',
        'description' => '<p style="text-align: justify;"><strong>Lorem ipsum</strong> dolor sit amet, consectetur adipisicing elit. Laboriosam mollitia aut eaque nihil eos rem suscipit error id nesciunt saepe? Deserunt aspernatur, iusto facere sequi doloribus accusantium porro odio alias.</p>
<p style="text-align: justify;">&nbsp;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam mollitia aut eaque nihil eos rem suscipit error id nesciunt saepe? Deserunt aspernatur, iusto facere sequi doloribus accusantium porro odio alias.</p>',
        'owner' => 'Alexandro',
        'service' => '<p style="text-align: justify;">Dinz Resort dengan pemandangan indah Gunung Salak, menawarkan beragam fasilitas dan pelayanan untuk memenuhi kebutuhan perjalanan bisnis dan liburan. bertempat di lokasi strategis di Pusat Pengembangan Bogor.</p>
<p style="text-align: justify;">Tempat yang sempurna untuk berakhir pekan, Menelusuri Kota Bogor dengan akses yang sangat mudah, itu akan sangat menarik dan menyenangkan untuk keluarga dan anak anak.</p>
<p style="text-align: justify;">Tempat terbaik untuk pertemuan bisnis dan grup, Dinz Resort terkenal sebagai pilihan utama dari penyelenggara konfrensi dikarenakan lokasinya yang strategis dan facilitas yang memuaskan.</p>
<p style="text-align: justify;"> </p>
<p style="text-align: justify;"><strong> "pengalaman kami menginap di dinz resort sangat memuaskan karena pelayanan dan fasilitas yang ada." - itok toni laksono</strong></p>
<div style="text-align: justify;"> </div>',
        'address' => 'Gedung Graha Satria 2 Lt. 5 Jl. RS. Fatmawati Raya No.5, Jakarta 12430, Indonesia',
        'footer' => 'Factory Washing Machine',
        'phone' => '(+62 21) 345321',
        'install' => '0',
        'province' => '6',
        'city' => '151',
        'public' => 'public',
        'secure' => false,
        'live' => '1',
        'contact' => '',
        'about' => '',
        'autonumber' => 12,
        'pagination' => 10,
        'button' => 55,
        'cache' => '10',
        'session' => '',
        'env' => 'local',
        'editor' => false,
        'latitude' => '-7.791972',
        'langitude' => '110.467309',
];
